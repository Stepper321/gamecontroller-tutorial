﻿using UnityEngine;
using System.Collections;

public class Changelevel : MonoBehaviour {

    public int capsToGive = 0;
    public string levelToLoad = string.Empty;
    public int teleporterID = 0;

    //I put a trigger collider on the CharacterController since the Character Controller component can for some reason not use OnColliderStay. So this will do with interacting for the player.
    void OnTriggerStay(Collider collision) //OnTrigger... use Collider and OnCollider use Collision. If you use Visual Studio you can just press CTRL+SHIFT+M to automatically create a MonoBehaviour function.
    {
        Debug.Log("collided with " + collision.gameObject);
        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("Giving player " + capsToGive + " caps. Teleporting player to " + levelToLoad + ", with teleportID " + teleporterID);
            GameController.gc.PlayerGiveCaps(capsToGive);
            GameController.gc.ChangeLevel(levelToLoad, teleporterID);
        }
    }


}
