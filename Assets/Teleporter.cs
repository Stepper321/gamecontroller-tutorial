﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour {

    [SerializeField]
    private int teleporterID = 0;


    //This is for privacy issues, I think it also avoids cheating but I'm not entirely sure.
    public int GetTeleporterID
    {
        get
        {
            return teleporterID;
        }
    }

    /// <summary>
    /// What I'm doing right here is disabling the Mesh Renderer when the scene starts. That is so that you don't see the purple spawner. Quite useful, right? Yes it is. Don't answer.
    /// You could also use Gizmos for this but I have no idea how to use that.
    /// </summary>
    void Start()
    {
        //Gets the Mesh Renderer. A var is pretty useful and can save some typing, but you have to initialize it immediately.
        var mesh = gameObject.GetComponent<MeshRenderer>();
        mesh.enabled = false;
        //^ and ofcourse disable the mesh. 
    }
	
}
