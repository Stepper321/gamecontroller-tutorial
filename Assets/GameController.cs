﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    int player_caps = 0;
    private bool teleportPlayer = false;
    private string currentLevel = string.Empty;

    /*
     * Create a static (which is readable from everywhere) GameController called gc. 
     */
    public static GameController gc;
    private GameObject Player;
    int teleportTo = 0;

    void Awake()
    {
        //initialize gc to be this exact script, so that other script can access it by typing GameController.gc.[function'
        gc = gameObject.GetComponent<GameController>();

        //Find the player
        Player = GameObject.FindGameObjectWithTag("Player");

        //This is fatal to a GameController, don't forget to tie this script on a GameController GameObject at the start of a script. 
        DontDestroyOnLoad(gameObject);

        ChangeLevel("level0", 0);
    }

	void Update () 
    {
        if (teleportPlayer)
        {
            //So it checks if it is the same level, and if it is, it won't do anything. This will keep looping until it hits a level with a different name.
            if (currentLevel == Application.loadedLevelName)
                return;

            //Get all Teleporters in the scene.
            GameObject[] teleporters = GameObject.FindGameObjectsWithTag("Teleporter");

            //a for each loop, if you don't know how that works research it a little.
            foreach (var teleporter in teleporters)
            {
                //It checks every teleporter in the teleporters array if it has the teleporterID equal of teleportTo.
                if (teleporter.GetComponent<Teleporter>().GetTeleporterID == teleportTo)
                {
                        
                    //If both the Player and Teleporter exist, (ie. if they're both not null) set the player's position to the teleporter's position.
                    if (Player && teleporter)
                    {
                        Player.transform.position = teleporter.transform.position;
                    }

                    //break out of the foreach loop.
                    break;
                }
            }

            //set it to false, so it won't be called anymore.
            teleportPlayer = false;
        }
	}
	

    //Pretty self explanatory.
    public void PlayerGiveCaps(int caps)
    {
        player_caps += caps;
    }

    //Before loading the level, the teleportPlayer boolean needs to get set to true, so that the player gets teleported on Scene Start.
    //Also set teleportTo to teleportID. (Which get's called from Teleporter.cs, or the Teleporter GameObject.
    //also, before it loads the level. It sets the currentLevel to the.. well.. current level.
    public void ChangeLevel(string level, int teleportID)
    {
        currentLevel = Application.loadedLevelName;
        teleportPlayer = true;
        teleportTo = teleportID;
        Application.LoadLevel(level);
    }

    //track player caps and put them on screen (top left).
    void OnGUI()
    {
        GUI.Label(new Rect(200, 15, 200, 25), "Player amount of caps: " + player_caps);
    }
}
